import java.util.Scanner;
public class BoardGameApp {
  public static void main(String[] args){
    Board b = new Board();
    int numCastles = 7;
    int turnsLeft = 8;
    Scanner read = new Scanner(System.in);
    System.out.print("Welcome, press [ENTER] to start\n");
    read.nextLine();

    while(numCastles > 0 && turnsLeft > 0){
      System.out.println( "Castles left: " + numCastles + "\nTurns left: " + turnsLeft + "\n" + b);

      System.out.print("Enter row number: ");
      int row = read.nextInt();

      System.out.print("Enter column number: ");
      int column = read.nextInt();
      System.out.println();

      int readGuess = b.placeToken(row, column);

      while(readGuess == -2 || readGuess == -1){
        System.out.print("\nCoordinates invalid, try again \nEnter row number: ");
        row = read.nextInt();

        System.out.print("Enter column number: ");
        column = read.nextInt();
        System.out.println();

        readGuess = b.placeToken(row, column);
      }

      if(readGuess == 0)
        numCastles--;
      turnsLeft--;
    }
    if (numCastles == 0)
      System.out.println("CONGRATS, you win");
    if (turnsLeft == 0)
      System.out.println("You Lose");

  }
}

