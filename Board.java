import java.util.Random;
public class Board{
    private Tile[][] board;

    public Board(){
        final int size = 5;
        Random rng = new Random();
        board = new Tile[size][size];
        
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board.length; j++){
                board[i][j] = Tile.BLANK;
            }
            board[i][rng.nextInt(size)] = Tile.HIDDEN_WALL;
        }
        
    }

    public String toString(){
        String result = "  0 1 2 3 4";
        for(int i = 0; i < board.length; i++){
            result += "\n" + i + " ";
            for(int j = 0; j < board.length; j++){
                result += board[i][j].getName() + " ";
            }
        }
        return result;
    }

    public int placeToken(int row, int column){
        if (row > board.length-1 || column > board[0].length-1 || row < 0 || column < 0)
            return -2;
        if (this.board[row][column] == Tile.CASTLE || this.board[row][column] == Tile.WALL)
            return -1;
        if (this.board[row][column] == Tile.HIDDEN_WALL){
            this.board[row][column] = Tile.WALL;
            return 1;
        }else {
            this.board[row][column] = Tile.CASTLE;
            return 0;
        }
    }
}